import axios from 'axios';

const unplash = axios.create({
  baseURL: "https://api.unsplash.com",
  headers: {
    
  }
});

export default unplash;